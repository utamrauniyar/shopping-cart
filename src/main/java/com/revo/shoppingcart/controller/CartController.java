package com.revo.shoppingcart.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.revo.shoppingcart.dao.CartDao;
import com.revo.shoppingcart.dao.CustomerDao;
import com.revo.shoppingcart.dto.CartDto;
import com.revo.shoppingcart.dto.CustomerDto;
import com.revo.shoppingcart.dto.ItemDto;

@Controller
public class CartController {

	@Autowired
	CartDao cartDao;

	@Autowired
	CustomerDao customerDao;

	@RequestMapping("/fetchitemsfromcart")

	public ModelAndView fetchItemsFromCart(HttpSession session) {
		CustomerDto c = (CustomerDto) session.getAttribute("customerinfo");

		CustomerDto customer = customerDao.findCustomerById(c.getId());
		CartDto cart = customer.getCart();
		List<ItemDto> items = cart.getItems();

		ModelAndView mav = new ModelAndView();
		mav.addObject("itemslist", items);
		mav.addObject("totalprice", cart.getTotalPrice());
		mav.setViewName("displaycartitemstocustomer");

		return mav;
	}
}
