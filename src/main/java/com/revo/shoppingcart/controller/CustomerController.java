package com.revo.shoppingcart.controller;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.revo.shoppingcart.dao.CustomerDao;
import com.revo.shoppingcart.dto.CustomerDto;

@Controller
public class CustomerController {
	@Autowired
	CustomerDao cdao;

	@RequestMapping("/addcustomer")
	public ModelAndView addCustomer() {
		CustomerDto c = new CustomerDto();
		ModelAndView mav = new ModelAndView();
		mav.addObject("customerobj", c);
		mav.setViewName("customerform");
		return mav;
	}

	@RequestMapping("/savecustomer")
	public ModelAndView saveCustomer(@ModelAttribute("customerobj") CustomerDto c) {
		cdao.saveCustomer(c);
		ModelAndView mav = new ModelAndView();
		mav.addObject("msg", "Registered successfully");
		mav.setViewName("customerloginform");
		return mav;
	}

	@RequestMapping("/customerloginvalidation")
	public ModelAndView loginValidation(ServletRequest req, HttpSession session) {
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		CustomerDto c = cdao.login(email, password);
		if (c != null) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("msg", "login ");
			mav.setViewName("customeroptions");
			session.setAttribute("customerinfo", c);
			return mav;
		} else {
			ModelAndView mav = new ModelAndView();
			mav.addObject("msg", "invalid CREDENTIAL");
			mav.setViewName("customerloginform");
			return mav;
		}
	}
}
