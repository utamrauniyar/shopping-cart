package com.revo.shoppingcart.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.revo.shoppingcart.dao.CartDao;
import com.revo.shoppingcart.dao.CustomerDao;
import com.revo.shoppingcart.dao.ItemDao;
import com.revo.shoppingcart.dao.ProductDao;
import com.revo.shoppingcart.dto.CartDto;
import com.revo.shoppingcart.dto.CustomerDto;
import com.revo.shoppingcart.dto.ItemDto;
import com.revo.shoppingcart.dto.ProductDto;

@Controller
public class ItemController {
	@Autowired
	ItemDao itemDao;

	@Autowired
	ProductDao productDao;

	@Autowired
	CartDao cartDao;

	@Autowired
	CustomerDao customerDao;

	@RequestMapping("/additem")
	public ModelAndView additem(@RequestParam("id") int id) {
		ProductDto p = productDao.findProductById(id);
		ModelAndView mav = new ModelAndView();
		mav.addObject("prodobj", p);
		mav.setViewName("itemform");
		return mav;
	}

	@RequestMapping("/additemtocart")
	public ModelAndView additem(ServletRequest req, HttpSession session) {
		int product_id = Integer.parseInt(req.getParameter("id"));
		String brand = req.getParameter("brand");
		double price = Double.parseDouble(req.getParameter("price"));
		String model = req.getParameter("moel");
		String category = req.getParameter("category");
		int quantity = Integer.parseInt(req.getParameter("quantity"));

		ItemDto item = new ItemDto();
		item.setBrand(brand);
		item.setPrice(price);
		item.setModel(model);
		item.setCategory(category);
		item.setQuantity(quantity);
		item.setProductId(product_id);
		item.setPrice(quantity * price);

		CustomerDto customer = (CustomerDto) session.getAttribute("customerinfo");
		CartDto c = customer.getCart();

		if (c == null) {
//			double totalPrice = 0;
			CartDto cart = new CartDto();
			List<ItemDto> items = new ArrayList<>();
			items.add(item);

			cart.setItems(items);
			cart.setName(customer.getName());

//			for (ItemDto i : items) {
//				totalPrice = totalPrice + i.getPrice();
//			}
//			cart.setTotalPrice(totalPrice);
			cart.setTotalPrice(item.getPrice());

			customer.setCart(cart);

			itemDao.saveItem(item);
			cartDao.saveCart(cart);

			customerDao.updateCustomer(customer);

		} else {
			List<ItemDto> items = c.getItems();
			if (items.size() > 0) {
				items.add(item);
				c.setItems(items);
				double totalPrice = 0;
				for (ItemDto i : items)
					totalPrice = totalPrice + i.getPrice();
				c.setTotalPrice(totalPrice);

				customer.setCart(c);

				itemDao.saveItem(item);
				cartDao.updateCart(c);
				customerDao.updateCustomer(customer);

			} else {
				List<ItemDto> itemsList = new ArrayList<>();
				itemsList.add(item);
				c.setItems(itemsList);
				c.setTotalPrice(item.getPrice());

				itemDao.saveItem(item);
				cartDao.updateCart(c);
				customerDao.updateCustomer(customer);
			}
		}
		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect://displayproducts");
		return mav;
	}
}
