package com.revo.shoppingcart.controller;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.revo.shoppingcart.dao.MerchantDao;
import com.revo.shoppingcart.dto.MerchantDto;

@Controller
public class MerchantController {
	@Autowired
	MerchantDao dao;

	@RequestMapping("/addmerchant")
	public ModelAndView addMerchant() {
		MerchantDto m = new MerchantDto();

		ModelAndView mav = new ModelAndView();
		mav.addObject("merchantobj", m);
		mav.setViewName("merchantform");
		return mav;
	}

	@RequestMapping("/savemerchant")
	public ModelAndView saveMerchant(@ModelAttribute("merchantobj") MerchantDto m) {
		dao.saveMerchant(m);

		ModelAndView mav = new ModelAndView();
		mav.addObject("message", "data saved successfully");
		mav.setViewName("menu");
		return mav;
	}

	@RequestMapping("/loginvalidation")
	public ModelAndView login(ServletRequest req, HttpSession session) {
		String email = req.getParameter("email");
		String password = req.getParameter("password");

		MerchantDto m = dao.login(email, password);
		ModelAndView mav = new ModelAndView();

		if (m != null) {
			mav.addObject("msg", "Successfully logged in");
			mav.setViewName("merchantoptions");
			session.setAttribute("merchantinfo", m);
			return mav;
		} else
			mav.addObject("msg", "invalid credentials");
		mav.setViewName("merchantloginform");
		return mav;
	}
}
