package com.revo.shoppingcart.controller;

import java.util.ArrayList;
import java.util.List;
import javax.print.attribute.standard.Sides;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.revo.shoppingcart.dao.CartDao;
import com.revo.shoppingcart.dao.CustomerDao;
import com.revo.shoppingcart.dao.OrderDao;
import com.revo.shoppingcart.dao.ProductDao;
import com.revo.shoppingcart.dto.CartDto;
import com.revo.shoppingcart.dto.CustomerDto;
import com.revo.shoppingcart.dto.ItemDto;
import com.revo.shoppingcart.dto.OrderDto;
import com.revo.shoppingcart.dto.ProductDto;

@Controller
public class OrderController {
	@Autowired
	OrderDao orderDao;

	@Autowired
	CustomerDao customerDao;

	@Autowired
	ProductDao productDao;

	@Autowired
	CartDao cartDao;

	@RequestMapping("/addorder")
	public ModelAndView addOrder() {
		OrderDto o = new OrderDto();
		ModelAndView mav = new ModelAndView();
		mav.addObject("ordersobj", o);
		mav.setViewName("ordersform");
		return mav;
	}

	@RequestMapping("/saveorder")
	public ModelAndView saveOrder(@ModelAttribute("ordersobj") OrderDto o, HttpSession session) {
		CustomerDto c = (CustomerDto) session.getAttribute("customerinfo");
		CustomerDto customer = customerDao.findCustomerById(c.getId());

		CartDto cart = customer.getCart();
		List<ItemDto> items = cart.getItems();

//		o.setTotalPrice(cart.getTotalPrice());

		List<ItemDto> itemsList = new ArrayList<ItemDto>();

		List<ItemDto> itemswithGreaterQuantity = new ArrayList<ItemDto>();

		for (ItemDto i : items) {
			ProductDto p = productDao.findProductById(i.getProductId());
			if (i.getQuantity() < p.getStock()) {
				itemsList.add(i);
				p.setStock(p.getStock() - i.getQuantity());

				productDao.updateProduct(p);
			} else {
				itemswithGreaterQuantity.add(i);
			}
		}
		o.setItems(itemsList);
		
		double totalpriceoforder = 0;
		
		for (ItemDto i : itemswithGreaterQuantity) {
			totalpriceoforder = totalpriceoforder + i.getPrice();
		}
		o.setTotalPrice(totalpriceoforder);
		cart.setItems(itemswithGreaterQuantity);

		double totalprice = 0;
		
		for (ItemDto i : itemswithGreaterQuantity) {
			totalprice = totalprice + i.getPrice();
		}
		cart.setTotalPrice(totalprice);

		CartDto updatedCart = cartDao.removeAllItemsFromCart(cart.getId());

		List<OrderDto> orders = customer.getOrders();
		if (orders.size() > 0) {
			orders.add(o);
			customer.setOrders(orders);
		} else {
			List<OrderDto> orders1 = new ArrayList<OrderDto>();
			orders1.add(o);
			customer.setOrders(orders1);
		}

		customer.setCart(updatedCart);

		orderDao.saveOrder(o);
		customerDao.updateCustomer(customer);

		ModelAndView mav = new ModelAndView();
		mav.addObject("msg", "order placed successfully");
		mav.addObject("orderdetails", o);
		mav.setViewName("customerBill");
		return mav;
	}
}
