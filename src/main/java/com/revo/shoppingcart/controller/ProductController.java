package com.revo.shoppingcart.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.revo.shoppingcart.dao.MerchantDao;
import com.revo.shoppingcart.dao.ProductDao;
import com.revo.shoppingcart.dto.MerchantDto;
import com.revo.shoppingcart.dto.ProductDto;

@Controller
public class ProductController {

	@Autowired
	ProductDao dao;

	@Autowired
	MerchantDao mdao;

	@RequestMapping("/addproduct")
	public ModelAndView addProduct() {
		ProductDto p = new ProductDto();
		ModelAndView mav = new ModelAndView();
		mav.addObject("productobj", p);
		mav.setViewName("productform");
		return mav;
	}

	@RequestMapping("/saveproduct")
	public ModelAndView saveProduct(@ModelAttribute("productobj") ProductDto p, HttpSession session) {
		MerchantDto merchant = (MerchantDto) session.getAttribute("merchantinfo");
		List<ProductDto> products = merchant.getProducts();
		if (products.size() > 0) {
			products.add(p);
			merchant.setProducts(products);
		} else {
			List<ProductDto> productsList = new ArrayList<ProductDto>();
			productsList.add(p);

			merchant.setProducts(productsList);
		}

		dao.saveProduct(p);
		mdao.updateMerchant(merchant);

		ModelAndView mav = new ModelAndView();
		mav.addObject("msg", "data saved successfully ");
		mav.setViewName("merchantoptions");
		return mav;
	}

	@RequestMapping("/deleteproduct")
	public ModelAndView deleteProduct(@RequestParam("id") int id, HttpSession session) {
		MerchantDto merchant = (MerchantDto) session.getAttribute("merchantinfo");

		MerchantDto m = mdao.deleteProductFromMerchant(merchant.getId(), id);

		mdao.updateMerchant(m);
		dao.deleteProductById(id);

		session.setAttribute("merchantinfo", m);

		ModelAndView mav = new ModelAndView();
		mav.addObject("msg", "data deleted successfully ");
		mav.setViewName("viewallproducts");
		return mav;

	}

	@RequestMapping("/displayproducts")
	public ModelAndView displayProducts() {
		List<ProductDto> products = dao.fetchAllProducts();
		ModelAndView mav = new ModelAndView();
		mav.addObject("productslist", products);
		mav.setViewName("viewallproductstocustomer");
		return mav;
	}

	@RequestMapping("/displayproductsbybrand")
	public ModelAndView displayProductsByBrand(ServletRequest req) {
		String brand = req.getParameter("brand");
		List<ProductDto> products = dao.findProductByBrand(brand);
		ModelAndView mav = new ModelAndView();
		mav.addObject("productslist", products);
		mav.setViewName("viewallproductstocustomer");
		return mav;
	}
}
