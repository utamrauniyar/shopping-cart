package com.revo.shoppingcart.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.revo.shoppingcart.dto.CustomerDto;

@Repository
public class CustomerDao {
	@Autowired
	EntityManagerFactory emf;

	public void saveCustomer(CustomerDto customer) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		et.begin();
		em.persist(customer);
		et.commit();
	}

	public CustomerDto login(String email, String password) {
		EntityManager em = emf.createEntityManager();

		Query query = em.createQuery("SELECT c FROM CustomerDto c WHERE c.email=?1 AND c.password=?2");
		query.setParameter(1, email);
		query.setParameter(2, password);

		try {
			CustomerDto customer = (CustomerDto) query.getSingleResult();
			return customer;
		} catch (NoResultException e) {
			return null;
		}

//		if (customer != null) {
//			return customer;
//		} else {
//			return null;
//		}
	}

	public void updateCustomer(CustomerDto c) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		et.begin();
		em.merge(c);
		et.commit();
	}

	public void deleteCustomerById(int id) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		CustomerDto c = em.find(CustomerDto.class, id);
		et.begin();
		em.remove(c);
		et.commit();
	}

	public CustomerDto findCustomerById(int id) {
		EntityManager em = emf.createEntityManager();

		CustomerDto c = em.find(CustomerDto.class, id);

		if (c != null) {
			return c;
		} else {
			return null;
		}
	}
}
