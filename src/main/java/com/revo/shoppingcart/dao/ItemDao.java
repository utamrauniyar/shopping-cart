package com.revo.shoppingcart.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.revo.shoppingcart.dto.ItemDto;

@Repository
public class ItemDao {
	@Autowired
	EntityManagerFactory emf;

	public void saveItem(ItemDto item) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.persist(item);
		et.commit();
	}

	public void updateItem(ItemDto item) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();
		
		et.begin();
		em.merge(item);
		et.commit();
	}

	public void deleteItemById(int id) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		ItemDto item = em.find(ItemDto.class, id);

		et.begin();
		em.remove(item);
		et.commit();
	}

	public ItemDto findItemById(int id) {
		EntityManager em = emf.createEntityManager();

		ItemDto item = em.find(ItemDto.class, id);

		if (item != null)
			return item;
		else
			return null;

	}
}
