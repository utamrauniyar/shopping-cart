package com.revo.shoppingcart.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.revo.shoppingcart.dto.MerchantDto;
import com.revo.shoppingcart.dto.ProductDto;

@Repository
public class MerchantDao {
	@Autowired
	EntityManagerFactory emf;

	@Autowired
	ProductDao pdao;

	public void saveMerchant(MerchantDto merchant) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		et.begin();
		em.persist(merchant);
		et.commit();
	}

	public MerchantDto login(String email, String password) {
		EntityManager em = emf.createEntityManager();

		Query query = em.createQuery("SELECT m FROM MerchantDto m WHERE m.email=?1 AND m.password=?2");
		query.setParameter(1, email);
		query.setParameter(2, password);

		try {
			MerchantDto merchant = (MerchantDto) query.getSingleResult();
			return merchant;
		} catch (Exception e) {
//			e.printStackTrace();
			return null;

		}
		/*
		 * if (merchant != null) { return merchant; } else { return null; }
		 */
	}

	public void updateMerchant(MerchantDto m) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		et.begin();
		em.merge(m);
		et.commit();
	}

	public void deleteMerchantById(int id) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		MerchantDto m = em.find(MerchantDto.class, id);
		et.begin();
		em.remove(m);
		et.commit();
	}

	public MerchantDto findMerchantById(int id) {
		EntityManager em = emf.createEntityManager();

		MerchantDto m = em.find(MerchantDto.class, id);

		if (m != null)
			return m;
		else
			return null;
	}

	public MerchantDto deleteProductFromMerchant(int merchant_id, int product_id) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		MerchantDto merchant = em.find(MerchantDto.class, merchant_id);
		List<ProductDto> products = merchant.getProducts();
//		products.stream().filter(product -> product.getId() != product_id).collect(Collectors.toList());
//		ProductDto p = pdao.findProductById(product_id);
//		products.remove(p);
//		merchant.setProducts(products);

//		et.begin();
//		em.remove(merchant);
//		et.commit();

		ArrayList<ProductDto> productList = new ArrayList<ProductDto>();
		for (ProductDto p : products) {
			if (p.getId() != product_id) {
				productList.add(p);
			}
		}
		merchant.setProducts(productList);

		return merchant;
	}
}
