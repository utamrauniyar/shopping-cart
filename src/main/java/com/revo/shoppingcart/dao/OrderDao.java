package com.revo.shoppingcart.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.revo.shoppingcart.dto.OrderDto;

@Repository
public class OrderDao {
	@Autowired
	EntityManagerFactory emf;

	public void saveOrder(OrderDto order) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		et.begin();
		em.persist(order);
		et.commit();
	}

	public void updateOrder(OrderDto order) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		et.begin();
		em.merge(order);
		et.commit();
	}

	public void deleteOrderById(int id) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		OrderDto order = em.find(OrderDto.class, id);

		et.begin();
		em.remove(order);
		et.commit();
	}

	public OrderDto findOrderById(int id) {
		EntityManager em = emf.createEntityManager();

		OrderDto order = em.find(OrderDto.class, id);

		if (order != null)
			return order;
		else
			return null;

	}
}
