package com.revo.shoppingcart.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.revo.shoppingcart.dto.ProductDto;

@Repository
public class ProductDao {
	@Autowired
	EntityManagerFactory emf;

	public void saveProduct(ProductDto product) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		et.begin();
		em.persist(product);
		et.commit();
	}

	public void updateProduct(ProductDto product) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		et.begin();
		em.merge(product);
		et.commit();
	}

	public void deleteProductById(int id) {
		EntityManager em = emf.createEntityManager();
		EntityTransaction et = em.getTransaction();

		ProductDto product = em.find(ProductDto.class, id);

		et.begin();
		em.remove(product);
		et.commit();
	}

	public ProductDto findProductById(int id) {
		EntityManager em = emf.createEntityManager();

		ProductDto product = em.find(ProductDto.class, id);

		if (product != null)
			return product;
		else
			return null;
	}

	public List<ProductDto> fetchAllProducts() {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT p FROM ProductDto p");
		return query.getResultList();
	}

	public List<ProductDto> findProductByBrand(String brand) {
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("SELECT p FROM ProductDto p WHERE p.brand=?1");
		query.setParameter(1, brand);
		return query.getResultList();
	}
}
