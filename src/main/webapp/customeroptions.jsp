<%@page import="com.revo.shoppingcart.dto.CustomerDto"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>customer options</title>
</head>
<body>
	<%
	CustomerDto c = (CustomerDto) session.getAttribute("customerinfo");
	if (c != null) {
	%>
	<h1>${msg }</h1>
	<h1>
		<a href="displayproducts">display all products</a>
	</h1>
	<h1>
		<a href="readbrandfromcustomer.jsp">display products by brand</a>
	</h1>
	<%
	} else {
	%>
	<h1>
		<a href="customerloginform.jsp">login first</a>
	</h1>
	<%
	}
	%>
</body>
</html>