<%@page import="java.util.List"%>
<%@page import="com.revo.shoppingcart.dto.ItemDto"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>display items t customer</title>
</head>
<body>
	<%
	List<ItemDto> items = (List<ItemDto>) request.getAttribute("itemslist");
	double totalprice = (Double) request.getAttribute("totalprice");
	%>
	<table>
		<th>Brand</th>
		<th>Model</th>
		<th>Category</th>
		<th>Price</th>
		<th>Quantity</th>

		<%
		for (ItemDto i : items) {
		%>
		<tr>
			<td><%=i.getBrand()%></td>
			<td><%=i.getModel()%></td>
			<td><%=i.getCategory()%></td>
			<td><%=i.getPrice()%></td>
			<td><%=i.getQuantity()%></td>
		</tr>
		<%
		}
		%>
	</table>
	<h2>
		Total Price:<%=totalprice%></h2>
	<br>
	<a href="addorder">Buy Now</a>
</body>
</html>