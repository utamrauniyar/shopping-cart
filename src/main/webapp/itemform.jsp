<%@page import="com.revo.shoppingcart.dto.ProductDto"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Item Form</title>
</head>
<body>
	<%
	ProductDto p = (ProductDto) request.getAttribute("prodobj");
	%>
	<form action="additemtocart">

		Id: <input type="hidden" name="id" value=<%=p.getId()%> readonly="true"> <br>
		Brand: <input type="text" name="brand" value=<%=p.getBrand()%> readonly="true"><br>
		Model: <input type="text" name="model" value=<%=p.getModel()%> readonly="true"><br>
		Category: <input type="text" name="category" value=<%=p.getCategory()%> readonly="true"><br>
		Price: <input type="text" name="price" value=<%=p.getPrice() %> readonly="true"><br>
		Quantity: <input type="number" name="quantity"><br>
		
		<input type="submit" value="Add To Cart">

	</form>
</body>
</html>