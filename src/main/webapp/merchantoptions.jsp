<%@page import="com.revo.shoppingcart.dto.MerchantDto"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>merchant options</title>
</head>
<body>

	<%
	MerchantDto m = (MerchantDto) session.getAttribute("merchantinfo");
	%>
	<%
	if (m != null) {
	%>
	<h1 style="color: green">${msg}</h1>
	<h2>
		<a href="addproduct">Add Product</a>
	</h2>
	<h2>
		<a href="viewallproducts.jsp">View All Products</a>
	</h2>
	<%
	} else {
	%>
	<h1>
		<a href="merchantloginform.jsp">Please Login First</a>
	</h1>
	<%
	}
	%>
</body>
</html>