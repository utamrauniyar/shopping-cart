<%@page import="com.revo.shoppingcart.dto.MerchantDto"%>
<%@page import="com.revo.shoppingcart.dto.ProductDto"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>view all products</title>
</head>
<body>
	<%
	MerchantDto merchant = (MerchantDto) session.getAttribute("merchantinfo");
	List<ProductDto> products = merchant.getProducts();
	%>

	<table cellpadding="20px" border="2px">
		<thead>
			<th>Id</th>
			<th>Brand</th>
			<th>Model</th>
			<th>Category</th>
			<th>Price</th>
			<th>Stock</th>
			<th>Update</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<%
			for (ProductDto p : products) {
			%>
			<tr>
				<td><%=p.getId()%></td>
				<td><%=p.getBrand()%></td>
				<td><%=p.getModel()%></td>
				<td><%=p.getCategory()%></td>
				<td><%=p.getPrice()%></td>
				<td><%=p.getStock()%></td>
				<td><a href="updateproduct">update</a></td>
				<td><a href="deleteproduct?id=<%=p.getId()%>">delete</a></td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>
</body>
</html>