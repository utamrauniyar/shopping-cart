<%@page import="com.revo.shoppingcart.dto.ProductDto"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>viewallproductstocustomer</title>
</head>
<body>
	<%
	List<ProductDto> products = (List<ProductDto>) request.getAttribute("productslist");
	%>
	<h1>
		<a href="fetchitemsfromcart">View Cart</a>
	</h1>
	<table cellpadding="20px" border="2px">
		<thead>
			<th>Id</th>
			<th>Brand</th>
			<th>Model</th>
			<th>Category</th>
			<th>Price</th>
			<th>Add to cart</th>
		</thead>
		<tbody>
			<%
			for (ProductDto p : products) {
			%>
			<tr>
				<td><%=p.getId()%></td>
				<td><%=p.getBrand()%></td>
				<td><%=p.getModel()%></td>
				<td><%=p.getCategory()%></td>
				<td><%=p.getPrice()%></td>
				<td><a href="additem?id=<%=p.getId()%>">add</a></td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>
</body>
</html>